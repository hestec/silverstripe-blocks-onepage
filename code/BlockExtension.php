<?php

class BlockExtension extends DataExtension {

    private static $db = array(
        'Anchor' => 'Varchar(50)'
    );

    public function updateCMSFields(FieldList $fields) {

        $fields->addFieldsToTab('Root.Main', array(

            TextField::create('Anchor', 'Anchor'),

        ));

        //$fields->addFieldToTab("Root.Settings", new TreeDropdownField('InternLinkID', 'Intern Link', 'SiteTree'));
    }

    public function generateURLSegment($anchor)
    {
        $filter = URLSegmentFilter::create();
        $urlSegment = $filter->filter($anchor);
        return $urlSegment;
    }

    public function onBeforeWrite() {

        if (strlen($this->owner->Anchor) > 1){

            $this->owner->Anchor = $this->generateURLSegment($this->owner->Anchor);

        }else{
            $this->owner->Anchor = $this->generateURLSegment($this->owner->Title);
        }


        parent::onBeforeWrite();

    }

    public function getTitleAnchor() {
        return $this->owner->Title." (".$this->owner->Anchor.")";
    }

}