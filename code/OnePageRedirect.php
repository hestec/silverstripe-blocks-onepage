<?php

class OnePageRedirect extends Page {

	private static $description = "Automatically redirects to the specified block at a specified page";

	private static $icon = "blocks-onepage/images/icons/onepageredirect.png";

	private static $has_one = array(
		"LinkToOnePage" => "SiteTree",
		"LinkToBlock" => "Block"
	);

	public function getCMSFields() {
    	$fields = parent::getCMSFields();

		$blocksSource = function($val) {
				$output = Block::get()->innerJoin("SiteTree_Blocks", "Block.ID = SiteTree_Blocks.BlockID")->where("SiteTree_Blocks.SiteTreeID = ".$val)->map('ID', 'getTitleAnchor')->toArray();

				//$output = $eric->getBlockList(1,false);
				return $output;
		};

		$linktoonepagefield = TreeDropdownField::create(
			"LinkToOnePageID",
			_t('BlocksOnePage.SELECT_THE_ONEPAGE', "Select the onepage"),
			"SiteTree"
		);

		$linktoblockidfield = DependentDropdownField::create('LinkToBlockID', _t('BlocksOnePage.BLOCK_ON_ONEPAGE', "Block on onepage"), $blocksSource);
		$linktoblockidfield->setDepends($linktoonepagefield);
		$linktoblockidfield->setEmptyString('('._t('BlocksOnePage.SELECT_BLOCK', "Select block").')');


		$fields->addFieldsToTab("Root.Main", array(
			$linktoonepagefield,
			$linktoblockidfield
		));
		$fields->removeFieldFromTab("Root.Main","Content");
		$fields->removeFieldFromTab("Root.Main","ReadMoreContentToggle");
 		
		return $fields;
   	}

	public function Link(){

		if ($this->LinkToOnePageID > 0){
			$onepagelink = SiteTree::get()->byID($this->LinkToOnePageID)->Link();
			if ($this->LinkToBlockID > 0){
				$blockanchor = Block::get()->byID($this->LinkToBlockID)->Anchor;
				$onepageblocklink = $onepagelink."#".$blockanchor;
			}else{
				$onepageblocklink = $onepagelink;
			}

			return $onepageblocklink;
		}
	}
		
}

class OnePageRedirect_Controller extends Page_Controller {

	function  init() {
		parent::init();

		$this->redirect(OnePageRedirect::Link());

	}

}
